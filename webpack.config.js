const path = require("path");
// const CleanWebpackPlugin = require("clean-webpack-plugin");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");

// CleanWebpackPlugin settings
/* let pathsToClean = ["assets"];

let cleanOptions = {
  root: __dirname,
  exclude: ["index.php"],
  verbose: true,
  dry: false
}; */

module.exports = {
  mode: "production",
  devtool: "source-map",
  entry: "./_dev/index.js",
  output: {
    filename: "js/custom.js",
    path: path.resolve(__dirname, "assets")
  },
  module: {
    rules: [
      {
        test: /\.(sa|sc|c)ss$/,
        use: [
          {
            loader: MiniCssExtractPlugin.loader
          },
          {
            loader: "css-loader"
          },
          {
            loader: "postcss-loader"
          },
          {
            loader: "sass-loader"
          }
        ]
      }
    ]
  },
  plugins: [
    // new CleanWebpackPlugin(pathsToClean, cleanOptions),
    new MiniCssExtractPlugin({
      filename: "css/custom.css"
    })
  ]
};
