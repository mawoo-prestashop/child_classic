// Theme style
import "./css/theme.scss";

// Home Slider
import HomeSlider from "./js/components/homeslider";
import SearchBar from "./js/components/ps_searchbar";
import MainMenu from "./js/components/ps_mainmenu";
import Header from "./js/components/header";

(() => {
  if (typeof $ === "undefined") {
    throw new TypeError("... requires jQuery.");
  }

  $(document).ready(() => {
    const homeSlider = new HomeSlider("#home-slider");
    const searchBar = new SearchBar();
    const mainMenu = new MainMenu();
    const header = new Header();

    homeSlider.init();
    searchBar.init();
    mainMenu.init();
    header.init();
  });
})();
