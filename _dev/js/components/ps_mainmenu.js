import Toggler from "../util/toggler";

class MainMenu {
  init() {
    Toggler.init(".js-top-menu-toggler", "#_top_menu", false);
    Toggler.init(".top-menu__sub-toggler", ".top-menu__sub-items", true);
  }
}

export default MainMenu;
