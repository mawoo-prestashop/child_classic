import MediaQueryList from "../util/media-query-list";

class Header {
  constructor() {
    this.breakpoint = "(min-width: 1200px)";
    this.scrollTop = 50;
    this.header = $("#header");
  }

  events() {
    const that = this;
    // let timeout;

    $(window).on("scroll", function() {
      // window.clearTimeout(timeout);

      // timeout = setTimeout(function() {
      that.shrink($(this));
      // }, 66);
    });
  }

  shrink(element) {
    let mqMatch = MediaQueryList.check(this.breakpoint);
    let scrollMatch = element.scrollTop() >= this.scrollTop;

    if (mqMatch) {
      if (scrollMatch) {
        this.header.addClass("shrink");
      } else {
        this.header.removeClass("shrink");
      }
    }
  }

  init() {
    this.events();
  }
}

export default Header;
