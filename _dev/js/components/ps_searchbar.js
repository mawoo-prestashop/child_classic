import Toggler from "../util/toggler";

class SearchBar {
  init() {
    Toggler.init(
      [".js-search-widget-toggler", ".js-search-widget-button-close"],
      "#search-widget-form",
      false
    );
  }
}

export default SearchBar;
