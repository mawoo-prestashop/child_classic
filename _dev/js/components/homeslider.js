class HomeSlider {
  constructor(sliderID) {
    (this.slickCDNUrl =
      "https://cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"),
      (this.sliderID = $(sliderID)),
      (this.settings = {
        arrows: true,
        prevArrow:
          '<button type="button" class="home-slider__arrow home-slider__arrow_prev" role="button"></button>',
        nextArrow:
          '<button type="button" class="home-slider__arrow home-slider__arrow_next" role="button"></button>',
        autoplay: true,
        autoplaySpeed: 5000,
        cssEase: "linear",
        dots: true,
        dotsClass: "home-slider__dots",
        fade: true,
        infinite: true,
        speed: 1000
      });
  }

  getSlickJS(url) {
    $.ajax({
      cache: true,
      dataType: "script",
      method: "GET",
      url: url
    })
      .done(() => {
        this.events();
        this.setHomeSliderHeight();
        this.initSlick();
        this.sliderID.addClass("active");
      })
      .fail(() => {
        throw new TypeError(
          "The slick script can not be downloaded. Home slider requires script (http://kenwheeler.github.io/slick/)."
        );
      });
  }

  events() {
    $(window).on("resize", () => {
      this.setHomeSliderHeight();
    });
  }

  getWindowHeight() {
    return $(window).height();
  }

  setHomeSliderHeight() {
    this.sliderID.css({
      height: this.getWindowHeight() + "px"
    });
  }

  initSlick() {
    this.sliderID.slick(this.settings);
  }

  init() {
    this.getSlickJS(this.slickCDNUrl);
  }
}

export default HomeSlider;
