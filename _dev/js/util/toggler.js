const Toggler = {
  events: (toggler, toggledElement, searchToggledElementInTogglerParent) => {
    $(document).on("click", toggler, function(event) {
      event.preventDefault();

      if (searchToggledElementInTogglerParent) {
        $(this)
          .toggleClass("active")
          .parent()
          .next()
          .slideToggle("fast");
      } else {
        $(this).toggleClass("active");
        $(toggledElement).toggleClass("active");
      }
    });
  },

  init(toggler, toggledElement, searchToggledElementInTogglerParent) {
    if (Array.isArray(toggler)) {
      const stringElementsInAnArray = toggler.join(",");

      this.events(
        stringElementsInAnArray,
        toggledElement,
        searchToggledElementInTogglerParent
      );
    } else {
      this.events(toggler, toggledElement, searchToggledElementInTogglerParent);
    }
  }
};

export default Toggler;
