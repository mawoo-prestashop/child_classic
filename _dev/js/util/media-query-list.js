const MediaQueryList = {
  check: mediaQueryString => {
    return window.matchMedia(mediaQueryString).matches;
  }
};

export default MediaQueryList;
