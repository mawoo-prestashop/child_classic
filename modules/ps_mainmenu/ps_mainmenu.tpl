{assign var=_counter value=0}
{function name="menu" nodes=[] depth=0 parent=null}
    {if $nodes|count}
      <ul {if $depth == 0}id="top-menu-main-items" class="top-menu__main-items"{else}class="top-menu__sub-items"{/if} data-depth="{$depth}">
        {foreach from=$nodes item=node}
            <li class="{if $depth == 0}top-menu__main-item{else}top-menu__sub-item{/if} {$node.type}{if $node.current} current {/if}" id="{$node.page_identifier}">
            {assign var=_counter value=$_counter+1}
              <a
                class="{if $depth >= 0}top-menu__dropdown-item{/if}{if $depth === 1} top-menu__dropdown-submenu{/if}"
                href="{$node.url}" data-depth="{$depth}"
                {if $node.open_in_new_window} target="_blank" {/if}
              >
                {if $node.children|count}
                  {* Cannot use page identifier as we can have the same page several times *}
                  {assign var=_expand_id value=10|mt_rand:100000}
                  <span class="top-menu__sub-toggler"></span>
                {/if}
                {$node.label}
              </a>
              {if $node.children|count}
                {menu nodes=$node.children depth=$node.depth parent=$node}
              {/if}
            </li>
        {/foreach}
      </ul>
    {/if}
{/function}

<div id="_top_menu" class="top-menu">
  <div class="container">
    {menu nodes=$menu.children}
  </div>
</div>
