{**
 * 2007-2018 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License 3.0 (AFL-3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/AFL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 * @author    PrestaShop SA <contact@prestashop.com>
 * @copyright 2007-2018 PrestaShop SA
 * @license   https://opensource.org/licenses/AFL-3.0 Academic Free License 3.0 (AFL-3.0)
 * International Registered Trademark & Property of PrestaShop SA
 *}

{if $homeslider.slides && $page.page_name == 'index'}
  <div id="home-slider" class="home-slider">
    {foreach from=$homeslider.slides item=slide name='homeslider'}
      <div class="home-slider__item {if $smarty.foreach.homeslider.first}active{/if}" role="option" aria-hidden="{if $smarty.foreach.homeslider.first}false{else}true{/if}" style="background-image: url({$slide.image_url});">
        {if $slide.title || $slide.description}
          <div class="container home-slider__container">
            <div class="home-slider__caption">
              <h2 class="home-slider__title">{$slide.title}</h2>
              <div class="home-slider__description">{$slide.description nofilter}</div>
              <a class="btn btn-primary home-slider__btn" href="{$slide.url}" title="{$slide.legend|escape}">{l s='View collection' d='Shop.Theme.Global'}</a>
            </div>
          </div>
        {/if}
      </div>
    {/foreach}
  </div>
{/if}
